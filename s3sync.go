package s3sync

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// SyncSet represents the file locations
type SyncSet struct {
	RootPath string
	Bucket   string
	Region   string
}

// SyncS3 copies all files from root to bucket in S3 region
func SyncS3(syncSet *SyncSet) {
	//log.Infof("Syncing S3: %q: %q", root, bucket)
	sess := session.New(&aws.Config{Region: aws.String(syncSet.Region)})
	uploader := s3manager.NewUploader(sess)

	visit := func(path string, f os.FileInfo, err error) error {
		if !f.IsDir() {
			key := path[len(syncSet.RootPath):len(path)]
			uploadFile(path, key, syncSet.Bucket, uploader)
		}
		return nil
	}

	filepath.Walk(syncSet.RootPath, visit)
}

// SyncWorkingRemote syncs from root to bucket in S3 region
func SyncWorkingRemote(syncSet *SyncSet, sumsFile, workingSumsPath, remoteSumsPath string) {
	//log.Infof("Syncing S3: %q: %q", root, bucket)
	sess := session.New(&aws.Config{Region: aws.String(syncSet.Region)})
	uploader := s3manager.NewUploader(sess)
	downloader := s3manager.NewDownloader(sess)

	addedFn := func(path string) {
		//log.Infof("File added: %q", path)
		uploadFile(syncSet.RootPath+path, path, syncSet.Bucket, uploader)
	}
	changedFn := func(path string) {
		//log.Infof("File changed: %q", path)
		uploadFile(syncSet.RootPath+path, path, syncSet.Bucket, uploader)
	}
	deletedFn := func(path string) {
		//log.Infof("File deleted: %q", path)
	}
	matchFn := func(path string) {
		//log.Debugf("File same: %q", path)
	}

	downloadFile(downloader, syncSet.Bucket, sumsFile, remoteSumsPath)
	syncWorkingRemoteWalker(workingSumsPath, remoteSumsPath, addedFn, changedFn, deletedFn, matchFn)
	uploadFile(workingSumsPath, sumsFile, syncSet.Bucket, uploader)
}

// CalcPathSums creates hashes the files in root to a summary file
func CalcPathSums(rootPath string, sumPath string) {
	//log.Infof("Calculating Sums: %q (to %q)", root, sumPath)
	sumsFile, err := os.Create(sumPath)
	if err != nil {
		panic(err)
	}
	defer sumsFile.Close()

	visit := func(path string, f os.FileInfo, err error) error {
		if !f.IsDir() {
			key := path[len(rootPath):len(path)]
			sum := calcFileSum(path)

			sumsFile.WriteString(key + " " + sum + "\n")
		}
		return nil
	}

	filepath.Walk(rootPath, visit)
}

func calcFileSum(path string) string {
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	hasher := sha256.New()
	if _, err := io.Copy(hasher, file); err != nil {
		panic(err)
	}
	return hex.EncodeToString(hasher.Sum(nil))
}

func syncWorkingRemoteWalker(workingSums, remoteSums string,
	addedFn, changedFn, deletedFn, matchFn func(string)) {
	workingSumMap := populateSumMap(workingSums)
	remoteSumMap := populateSumMap(remoteSums)

	for k, workingSum := range workingSumMap {
		remoteSum, ok := remoteSumMap[k]

		if !ok {
			addedFn(k)
		} else if strings.Compare(workingSum, remoteSum) != 0 {
			changedFn(k)
		} else {
			matchFn(k)
		}
		delete(remoteSumMap, k)
	}

	for k := range remoteSumMap {
		deletedFn(k)
	}
}

func populateSumMap(path string) (sumMap map[string]string) {
	sumMap = make(map[string]string)

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	var lineSum, linePath string
	for {
		_, err := fmt.Fscanf(file, "%s %s\n", &linePath, &lineSum)
		if err == io.EOF {
			return
		} else if err != nil {
			panic(err)
		}

		sumMap[linePath] = lineSum
	}
}

func uploadFile(path string, key string, bucket string, uploader *s3manager.Uploader) {
	//log.Debugf("Syncing %q from %q", key, path)

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	upParams := &s3manager.UploadInput{
		Bucket: &bucket,
		Key:    &key,
		Body:   file,
	}
	_, err = uploader.Upload(upParams)
	if err != nil {
		panic(err)
	}
}

func downloadFile(downloader *s3manager.Downloader, bucket string, key string, path string) {
	//log.Debugf("Syncing %q to %q\n", key, path)

	file, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	_, err = downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(key),
		})
	if err != nil {
		if awserr, ok := err.(awserr.Error); ok {
			if awserr.Code() != "NoSuchKey" {
				panic(err)
			}
		} else {
			panic(err)
		}
	}
}
